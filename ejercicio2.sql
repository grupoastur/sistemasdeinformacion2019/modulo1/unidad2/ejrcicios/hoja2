﻿USE ciclistas;

-- 1 numero de ciclistas por equipo
    SELECT AVG( c.edad),  c.nomequipo, COUNT(*) numCiclistas FROM ciclista c GROUP BY c.nomequipo; 

-- 2 númrero de etapas que ha ganado cada uno de los ciclistas
      SELECT e.dorsal, e.numetapa FROM etapa e GROUP BY e.dorsal; 

-- 3 el dorsal de los ciclistas que hayan  ganado más de 1 etapa
      SELECT e.dorsal FROM etapa e GROUP BY e.dorsal HAVING COUNT(*)>1;  
      
-- 4 listar edades de todos los ciclistas de Banesto
      SELECT c.nombre, c.edad FROM ciclista c WHERE c.nomequipo='Banesto'; 
      
-- 5 listar las edades de los ciclistas que son del Banesto o de Navigare
      SELECT c.edad FROM ciclista c WHERE c.nomequipo='Banesto' OR c.nomequipo='Navigare';
      
-- 6 listar el dorsal de los ciclistas que son del Banesto y cuya edad sea entre 25 y 32    
      SELECT c.nombre, c.dorsal  FROM ciclista c WHERE c.nomequipo='Banesto' AND c.edad BETWEEN 25 AND 32;
      
-- 7 listar el dorsal de los ciclistas que son del Banesto o cuya edad sea entre 25 y 32 
      SELECT c.nombre, c.dorsal FROM ciclista c WHERE c.nomequipo='Banesto' OR c.edad BETWEEN 25 AND 32;
      
-- 8 listar la inicial del equipo de los ciclistas cuyo nombre comience por r
      SELECT DISTINCT c.nombre, LEFT( c.nomequipo,1) FROM ciclista c WHERE c.nombre LIKE 'R%';  
     
-- 9 listar el código de las etapas que su salida y llegada sea en la misma población
      -- esta esta mal
      SELECT e.salida, e.llegada,e.numetapa FROM etapa e GROUP BY e.salida= e.numetapa AND e.llegada= e.numetapa;
      -- esta esta bien
      SELECT e.salida, e.llegada, e.numetapa FROM etapa e WHERE e.salida= e.llegada; 
      
-- 10  cuantos ciclistas han ganado mas de dos etapas
      SELECT c.nombre  FROM   (SELECT e.dorsal  FROM etapa e GROUP BY e.dorsal HAVING COUNT(*)>2) c1 JOIN ciclista c USING (dorsal);  
      
-- 11 el nombre del ciclista que tiene más edad
      SELECT * FROM ciclista c;                
      -- c1 : edad maxima
        SELECT MAX( c.edad) maxima FROM ciclista c;
        -- solucion : join
          SELECT (c.nombre) CiclistaDe, (c.edad) MáximaEdad FROM ciclista c JOIN (SELECT MAX( c.edad) maxima FROM ciclista c) c1 ON c1.maxima= c.edad;    
      
-- 12 edad media de los ciclistas por cada equipo
        SELECT AVG( c.edad) EdadMedia, c.nomequipo FROM ciclista c GROUP BY c.nomequipo ; 
      
-- 13 numero de ciclistas por equipo
          SELECT c.nomequipo, COUNT(*) FROM ciclista c GROUP BY c.nomequipo; 

-- 14 numero de puertos
        SELECT COUNT(*) FROM puerto p; 

-- 15 numero total de puertos mayores de 1500 metros
        SELECT COUNT(*) FROM  puerto p WHERE p.altura>1500;
        
-- 16 listar el nombre de los equipos que tengan mas de 4 ciclistas
        SELECT c.NomEquipo, COUNT(*) NumCiclistas FROM   ciclista c GROUP BY c.nomequipo HAVING COUNT(*)>4;
        -- con alias
        SELECT COUNT(*) nciclistas, c.nomequipo FROM ciclista c GROUP BY c.nomequipo HAVING nciclistas>4;
        
-- 17 listar el nombre de los equipos que tengan mas de 4 ciclistas cuya edad este entre 28 y 32 años
       SELECT c.nombre, c.edad, COUNT(*) ciclistas, c.nomequipo FROM ciclista c WHERE c.edad BETWEEN 28 AND 32 GROUP BY c.nomequipo HAVING ciclistas>4;

-- 18 indicame el numero de etapas que ha ganado cada uno de los ciclistas
        SELECT e.dorsal, COUNT(*) EtapaGanada FROM  etapa e GROUP BY e.dorsal; 
        
-- 19 indicame el dorsal de los ciclistas que hayan ganado mas de una etapa
          SELECT COUNT(*) ganadas , e.dorsal FROM etapa e GROUP BY e.dorsal HAVING ganadas>1;
  